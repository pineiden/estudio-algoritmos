use math::round;

fn insertion_sort(lista: &mut Vec<i32>){
    let largo = lista.len();
    for j in 1..largo{
        let mut i = j;
        while i > 0 && lista[i-1] > lista[i] {
            lista.swap(i-1, i);
            i -= 1;
        }
    }
}

fn insertion_sort_decreciente(lista: &mut Vec<i32>){
    /*
    largo =  len(lista)
    for j in range(largo-1,-1,-1):
    valor =  lista[j]
    #indice anterior
    i = j + 1
    while i < largo and lista[i] < valor:
    lista[i - 1] = lista[i]
    i = i + 1
    lista[i-1] = valor
     */
    let largo = lista.len();
    for j in (0..largo-1).rev() {
        let mut i = j;
        while i < largo && lista[i+1] > lista[i] {
            lista.swap(i + 1, i);
            i += 1;
        }
    }
}

use std::f64;


fn merge(A: &mut Vec<f64>, p: usize, q: usize, r: usize){
    /*
    Slice left
     */
    let mut L = &A[p..q+1];
    let mut R = &A[q..r+1];
    let mut i: u32 = 0;
    let mut j: u32 = 0;
    let inf = f64::INFINITY;
    L.push(inf);
    R.push(inf)
}

fn merge_sort(A: &mut Vec<f64>, p: usize, r: usize){
    if p < r
    {
        let q: usize = round::floor((p+r)/2,0) as usize;
        merge_sort(A, p, q);
        merge_sort(A, q+1, r);
        merge(A, p, q, r);
    }
}



fn main() {
    let mut lista = vec![3,-5, 8, 9, 50, 33,1000,-34532];
    println!("{:?}", lista);
    // le envio la referencia a direccion de lista,
    // también le digo que es mutable
    insertion_sort(&mut lista);
    println!("Creciente {:?}", lista);
    let mut lista = vec![3,-5, 8, 9, 50, 33,1000,-34532];
    insertion_sort_decreciente(&mut lista);
    println!("Decreciente {:?}", lista);
}
