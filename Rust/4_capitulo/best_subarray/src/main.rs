
/*
Se tiene una lista de valores y se debe encontrar el tramo que 
permita maximizar la ganancia, considerando que 
1.- se compra primero 'accion'
2.- se vende
El subarray encontrado será el momento de compra para i=0
y de venta para i=n
 */
use std::f64;
/*
find_max_crossing_subarray
 */

fn find_max_crossing_subarray(array: &Vec<f64>,
                              low: usize,
                              mid: usize,
                              high:usize)->(usize,usize,f64){
	let mut left_sum = f64::NEG_INFINITY;
	let mut _sum = 0.0;
  let mut max_left:usize = 0;
	  for i in (low..mid+1).rev(){
		_sum += array[i];
		if _sum > left_sum {
			  left_sum = _sum;
			  max_left = i;
    }
  }
	let mut right_sum = -f64::INFINITY;
  let mut max_right:usize = 0;
	  _sum = 0.0;
	for j in mid+1..high{
		_sum += array[j];
		if _sum > right_sum{
			  right_sum = _sum;
			  max_right = j;
    }
  }
  (max_left, max_right, left_sum+right_sum)
}
/*
find_maximun_subarray
 */
fn find_maximun_subarray(array: &Vec<f64>, low: usize, high: usize)->(usize, usize, f64){
	if low == high {
		  (low, high, array[low])
  }
	else{
		let mid: usize = (((low + high) as f32)/2.0).floor() as usize;
		let (left_low, left_high, left_sum) = find_maximun_subarray(array, low, mid);
		let (right_low, right_high, right_sum) = find_maximun_subarray(array, mid+1, high);
		let (cross_low, cross_high, cross_sum) = find_max_crossing_subarray(array, low, mid, high);
    if left_sum >= right_sum && right_sum>=cross_sum
    {
        (left_low, left_high, left_sum)
    }
    else if right_sum >= left_sum && right_sum >= cross_sum
        {
            (right_low, right_high, right_sum)
        }
		else{
			  (cross_low, cross_high, cross_sum)
    }
  }
}

fn values_to_changes(values: &Vec<f64>)->Vec<f64>{
    let mut changes = vec![];
    let len = values.len();
    for (pos,value) in values[..len-1].iter().enumerate() {
        let dif = values[pos+1]-value;
        changes.push(dif);
    }
    changes
}

fn main() {
    let values = vec![
      100.0,113.0,110.0,85.0,105.0,102.0,86.0,
      63.0,81.0,101.0,94.0,106.0,101.0,79.0,94.0,90.0,97.0
    ];
    let changes = values_to_changes(&values);
    println!("Values {:?}", values);
    println!("Changes {:?}", changes);
    let (low, high, value) =find_maximun_subarray(&changes, 0, changes.len()-1);
    println!("En changes: Low:{} High:{} Value:{}",low, high, value);
    println!("En values: Low:{} High:{} Value:{}",low+1, high+1, value);
    println!("Pero como va!");
}
