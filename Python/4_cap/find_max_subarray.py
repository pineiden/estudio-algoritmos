"""
Se tiene una lista de valores y se debe encontrar el tramo que 
permita maximizar la ganancia, considerando que 
1.- se compra primero 'accion'
2.- se vende
El subarray encontrado será el momento de compra para i=0
y de venta para i=n
"""
import math

NINF = - math.inf
INF = math.inf

def find_max_crossing_subarray(A, low, mid, high):
	left_sum = NINF
	_sum = 0
	for i in range(mid, low-1, -1):
		_sum += A[i]
		if _sum > left_sum:
			left_sum = _sum
			max_left = i
	right_sum = NINF
	_sum = 0
	for j in range(mid+1, high+1):
		_sum += A[j]
		if _sum > right_sum:
			right_sum = _sum
			max_right = j
	return (max_left, max_right, left_sum+right_sum)		
	
def find_maximun_subarray(A, low, high):
	if low == high:
		return (low, high, A[low])
	else:
		mid= math.floor(sum([low, high])/2)
		(left_low, left_high, left_sum) = find_maximun_subarray(A, low, mid)
		(right_low, right_high, right_sum) = find_maximun_subarray(A, mid+1, high)
		(cross_low, cross_high, cross_sum) = find_max_crossing_subarray(A, low, mid, high)
		if left_sum >= right_sum and right_sum>=cross_sum:
			return (left_low, left_high, left_sum)
		elif right_sum >= left_sum and right_sum >= cross_sum:
			return (right_low, right_high, right_sum) 
		else:
			return (cross_low, cross_high, cross_sum)
		
	
	
if __name__ == '__main__':
	values = [
		100,113,110,85,105,102,86,
		63,81,101,94,106,101,79,94,90,97
	]
	print(values)
	changes = []
	for i,v in enumerate(values[:-2]):
		changes.append(values[i+1]-v)
	print(changes)
	# Se calcula sobre changes::::
	result = find_maximun_subarray(changes, 0, len(changes)-1)
	print(*zip(["index-0", "index-1", "suma"],result))
